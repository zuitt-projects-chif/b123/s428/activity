const fullNameHeading = document.querySelector("#fullNameHeading");
const firstNameInput = document.querySelector("#firstName");
const lastNameInput = document.querySelector("#lastName");

const updateNames = () => {
  fullNameHeading.innerHTML = `${firstNameInput.value} ${lastNameInput.value}`;
};

firstNameInput.addEventListener("keyup", updateNames);
lastNameInput.addEventListener("keyup", updateNames);

const submitBtn = document.querySelector("#submitBtn");
submitBtn.addEventListener("click", () => {
  if (firstNameInput.value === "" || lastNameInput.value === "") {
    alert("Please input first and lastname");
    return;
  }
  alert(
    `Thank you for registering ${firstNameInput.value} ${lastNameInput.value}`
  );
});
